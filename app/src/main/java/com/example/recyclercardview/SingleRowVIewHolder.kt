package com.example.recyclercardview

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SingleRowVIewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val textData=itemView.findViewById<TextView>(R.id.text_title)
    val textDescription=itemView.findViewById<TextView>(R.id.text_description)
    val imgIcon=itemView.findViewById<ImageView>(R.id.img_view)

}