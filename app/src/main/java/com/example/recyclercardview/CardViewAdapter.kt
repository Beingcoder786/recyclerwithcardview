package com.example.recyclercardview

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Adapter
import androidx.recyclerview.widget.RecyclerView

class CardViewAdapter(val model:List<Model>): RecyclerView.Adapter<SingleRowVIewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SingleRowVIewHolder {

        val view=LayoutInflater.from(parent.context).inflate(R.layout.single_row,parent,false)
        return SingleRowVIewHolder(view)
    }

    override fun getItemCount(): Int {

    return model.size
    }

    override fun onBindViewHolder(holder: SingleRowVIewHolder, position: Int) {

        holder.imgIcon.setImageResource(model[position].img)
        holder.textData.text=model[position].textTitle
        holder.textDescription.text=model[position].textDescription

    }
}