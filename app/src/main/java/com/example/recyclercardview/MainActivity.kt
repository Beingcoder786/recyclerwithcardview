package com.example.recyclercardview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val dataHolder = mutableListOf(
            Model(R.drawable.cp,"C Programming", "Desktop Programming"),
            Model(R.drawable.cpp,"C++ Programming", "Desktop Progamming Language"),
            Model(R.drawable.java,"Java Programming", "Desktop Progamming Language"),
            Model(R.drawable.php,"PHP Programming", "Web Progamming Language"),
            Model(R.drawable.dotnet,".NET Programming", "Desktop/Web Progamming Language"),
            Model(R.drawable.wordpress,"Wordpress Framework", "PHP based Blogging Framework"),
            Model(R.drawable.magento,"Magento Framework", "PHP Based e-Comm Framework"),
            Model(R.drawable.shopify,"Shopify Framework", "PHP based e-Comm Framework"),
            Model(R.drawable.angular,"Angular Programming", "Web Programming"),
            Model(R.drawable.python,"Python Programming", "Desktop/Web based Progamming"),
            Model(R.drawable.nodejs,"Node JS Programming", "Web based Programming")

        )


        val recyclerView=findViewById<RecyclerView>(R.id.recycler_view)
        val adapter = CardViewAdapter(dataHolder) //object create of adaptter
        recyclerView.adapter = adapter//link recycler to adapter

        recyclerView.layoutManager=LinearLayoutManager(this)



    }
}